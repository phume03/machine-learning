#! /usr/lib/env python3.5

import sklearn
import numpy as number
import math
from sklearn.datasets.california_housing import fetch_california_housing # CA Census Data

print("Checking datasets.\n\n")
[dataset for dataset in sklearn.datasets.__dict__.keys() if str(dataset).startswith("fetch_") or str(dataset).startswith("load_")]
print("\n\n")

houses = fetch_california_housing() # CA Census Data
print(houses.DESCR)
houses.feature_names
houses.data.shape
houses.data[100]
math.exp(houses.target[0])

# SAMPLING CONCEPTS
# US POPULATION vs US SAMPLE
# CA POPULATION vs CA SAMPLE
# CA POPULATION is a sample of the US Popuplation but very biased (i.e. it is not representative of the US population).
sample = houses.data.shape[0]
sample

# AI/ML training data set (to fit any model); 70% of CA population
train_start = 0
train_end = 0.7 * sample
train_inputs = houses.data[:int((0.75) * sample), :]
train_labels = houses.target[:int((0.75) * sample)]
# CV (cross validation) data set; 15% of CA population
cv_start = 0.7 * sample
cv_end = 0.85 * sample # note 0.85 - 0.7 = 0.15 => 15%
cv_input = houses.data[int((0.75) * sample): int((0.85) * sample), :]
cv_labels = houses.target[int((0.75) * sample): int((0.85) * sample)]
# Test data set (used once); 15% of CA population
test_start = 0.85 * sample
test_end = 1.0 * sample
test_inputs = houses.data[int((0.85) * sample):, :]
test_labels = houses.target[int((0.85) * sample):]


# The above can also be done as follows, courtesy of http://www.clungu.com/scikit-learn/tutorial/Scikit-learn/
slice_points = [0, 0.7, 0.85, 1]
slice_points = list(zip(slice_points, slice_points[1:]))
slice_points
slice_points = number.array(slice_points) * samples
slice_points

train_inputs, cv_inputs, test_inputs = [houses.data[int(start):int(stop)] for start, stop in slice_points]
train_labels, cv_labels, test_labels = [houses.target[int(start): int(stop)] for start, stop in slice_points]

train_inputs.shape, cv_inputs.shape, test_inputs.shape 
assert train_inputs.shape[0] == train_labels.shape[0]
assert valid_inputs.shape[0] == valid_labels.shape[0]
assert tests_inputs.shape[0] == tests_labels.shape[0]

# Split differently
train_inputs, test_inputs, train_labels, test_labels = sklearn.cross_validation.train_test_split(houses.data, houses.target, test_size=0.30)
cv_inputs, test_inputs, cv_labels, test_labels = sklearn.cross_validation.train_test_split(rest_inputs, rest_labels, test_size=0.5)









#MACHINE LEARNING
-----

Playing with _python_ AI/machine learning tools. Algorithms in this file are taken directly from the machine learning tutorial found in the pdf in this repository, also found on [Digital Ocean] (http://assets.digitalocean.com/books/python/machine-learning-projects-python.pdf). If you have a vested interest in Machine Learning, continue and learn from experts at [Facebook](https://engineering.fb.com/ai-research/ai-revealed/) and [other independent scientists](http://alvinwan.com/).

There is a missing link for extra images outside of the mnist database to test our database on. After doing a bit of research online, I found the following [NIST](https://www.nist.gov/services-resources/software/public-domain-ocr) website to be informative, and this other one by [lionbridge] (https://lionbridge.ai/datasets/15-best-ocr-handwriting-datasets/) to contain a broad sample. I also found a different tutorial on [MXNET] (http://mxnet.incubator.apache.org/test/versions/0.10/tutorials/python/mnist.html) to help you on your journey of discovery.


* Machine Learning 1: Introduction to Machine Learning

* Machine Learning 2: Exploring Neural Networks

* Machine Learning 3:  Machine Learning (ML): the gym package contains a number
of game environments. These environments or games are the classic games that 
were created by the first programmers on the first computers. This script, in 
separate parts, initiates a gaming environment, and shows us how to use tools 
built into the gym package to read the gaming environment at a particular point
in time (also known as a state - Theory of Computation). 

Five different machine learning experiments are explored in this section, namely; CartPole Bot, Space Invaders Bot, Frozen Lake Bot, Ms Pacman, and Hopper Bot.


## Useful Notes

## Use Instructions
The following are linux/unix install instructions.

### Python 3.5
    sudo apt-get update

    sudo apt-get install python3.5 python3.5-dev python3.5-doc python3.5-examples python3.5-venv

*Note:* python3.5 is required by some packages.

### Python Dependencies e.g PIP (required to install and run other packages)
    sudo apt-get install python3-pip python3-wheel python-pil

### Python Packages: Numpy, Scipy, Scikit-learn
    sudo pip3 install --upgrade pip

Confirm that the right version of pip has been installed, and it is attached or pointing to the python3.5 (or later) interpreter and not any other version of python, by running the following command:

    sudo pip3 -V 

This matters because tensorflow, for example, will not run on any [version of pip lower than 19.0](https://www.tensorflow.org/install). If all is inorder then you may install the needed packages as follows:

    sudo pip3 install numpy scipy sklearn tensorflow tensorflow-cpu tensorflow-plot tensorflow-datasets gym opencv-python

OR

    sudo python3.5 -m pip install numpy scipy sklearn tensorflow tensorflow-cpu tensorflow-plot tensorflow-datasets gym opencv-python

Remember to check the project dependencies. Scripts with missing dependencies will break, so pay attention to the log message, and resolve it accordingly. For example, on testing the gym package library, I learned that some environments do  not come installed with the default gym package. So, I had to resolve them with the following code:

    sudo pip3 install gym[atari]


----

You may wish to use other python tools to perform some of the operations completed by my scripts. Feel free to do further research on python and download, install, and employ the necessary tools. You may find this more rewarding than doing things exactly as I have.

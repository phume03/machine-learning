#! /usr/lib/env python3.5
"""
 Deep Learning, Reinforcement Learning, and Deep Reinforcement Learning. 

 1 CartPole Bot: reads the environment showing us the basic metrics produced
 2 Frozen Lake Bot: advancing our understanding of the first two bots above, in 
 what is known as reinforcement learning. That is, environments change when 
 these change, data is produced. The parent field of rl is control theory - 
 which proposes controlling dynamic systems. An application of control theory is
 self driving cars, the road in the immediate periphery of the AI is an 
 environment. Whether the car moves or other objects move, the AI should read 
 the environment before it can react to it. Instead of removing the obstacle, 
 the AI in this example, will most likely move itself. This is a summary of 
 the introduction to an essay on "Reinforcement Learning" by Mark Drake.
 3 Ms Pacman: Deep Reinforcement Learning.
 4 Hopper: Method of Least Squares/Regression Analysis.
 5 Space Invaders Bot: reads the environment and starts to manipulate some metrics for our benefit.
"""
from typing import Tuple
from typing import Callable
from typing import List
import gym
import numpy as np
import random
import tensorflow as tf

global g_num, seed
num_episodes = 5000
discount_factor = 0.85
learning_rate = 0.9
w_lr = 0.5
report_interval = 500
exploration_probability = lambda episode: 50. / (episode + 10)
report = '100-ep Average: %.2f . Best 100-ep Average: %.2f . Average: %.2f (Episode %d)'
seed = False

def initialize(shape: Tuple):
  """ Initialize Model """
  W = np.random.normal(0.0, 0.1, shape)
  Q = makeQ(W)
  return W, Q

def game(x):
  # == Create Game Environment ==
  random.seed(0)
  np.random.seed(0)
  tf.set_random_seed(0)
  env = ""
  if x==1:
    env = gym.make('CartPole-v0')
    pass
  elif x==2:
    # env = gym.make('MsPacman-v0')
    env = gym.make('FrozenLake-v0')
    pass
  elif x==3:
    env = gym.make('FrozenLake-v0')
    pass
  elif x==4:
    #env = gym.make('Hopper-v1')  
    env = gym.make('FrozenLake-v0')
    pass
  elif x==5:
    env = gym.make('SpaceInvaders-v0')
    pass
  else:
    # 6 env = gym.make('MountainCar-v0')
    pass

  if seed==True:
    env.seed(0)
    pass
  return env

def loop(x, env): 
  if x==1:
    #
    for i_episode in range(num_episodes):
      observation = env.reset()
      for t in range(100):
        env.render()
        print(observation)
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        if done:
          print("Episode finished after {} timesteps.".format(t+1))
          break
        pass
      pass
    env.close()
    pass
  elif x==2:
    # print('Ms Pacman')
    # print("Deep Frozen Lake")
    rewards = []
    n_obs, n_actions = env.observation_space.n, env.action_space.n
    obs_t_ph = tf.placeholder(shape=[1, n_obs], dtype=tf.float32)
    obs_tp1_ph = tf.placeholder(shape=[1, n_obs], dtype=tf.float32)
    act_ph = tf.placeholder(tf.int32, shape=())
    rew_ph = tf.placeholder(shape=(), dtype=tf.float32)
    q_target_ph = tf.placeholder(shape=[1, n_actions], dtype=tf.float32)

    W = tf.Variable(tf.random.uniform([n_obs, n_actions], 0, 0.01))
    q_current = tf.matmul(obs_t_ph, W)
    q_target = tf.matmul(obs_tp1_ph, W)

    q_target_max = tf.reduce_max(q_target_ph, axis=1)
    q_target_sa = rew_ph + discount_factor * q_target_max
    q_current_sa = q_current[0, act_ph]
    error = tf.reduce_sum(tf.square(q_target_sa - q_current_sa))
    pred_act_ph = tf.argmax(q_current, 1)

    trainer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
    update_model = trainer.minimize(error)

    # Q = np.zeros((env.observation_space.n, env.action_space.n))
    with tf.Session() as session:
      session.run(tf.global_variables_initializer())
      for episode in range(1, num_episodes+1):
        obs_t = env.reset()
        episode_reward = 0
        while True:
          # noise = np.random.random((1, env.action_space.n)) / (episode**2.)
          # env.render()
          obs_t_oh = one_hot(obs_t, n_obs)
          action = session.run(pred_act_ph, feed_dict={obs_t_ph: obs_t_oh})[0]
          if np.random.rand(1) < exploration_probability(episode):
            action = env.action_space.sample()
            pass
          obs_tp1, reward, done, info = env.step(action)
          obs_tp1_oh = one_hot(obs_tp1, n_obs)
          q_target_val = session.run(q_target, feed_dict={obs_tp1_ph: obs_tp1_oh})
          session.run(update_model, feed_dict={obs_t_ph: obs_t_oh, rew_ph: reward, q_target_ph: q_target_val, act_ph: action})
          episode_reward += reward
          obs_t = obs_tp1
          if done:
            # print('Reward: %s' % episode_reward)
            rewards.append(episode_reward)
            if episode % report_interval == 0:
              print_report(rewards, episode)
              pass
            break
        pass
      pass
    env.close()
    print_report(rewards, -1)
    pass
  elif x==3:
    # print('Frozen Lake')
    rewards = []
    Q = np.zeros((env.observation_space.n, env.action_space.n))
    for episode in range(1, num_episodes+1):
      current_state = env.reset()
      episode_reward = 0
      while True:
        noise = np.random.random((1, env.action_space.n)) / (episode**2.)
        #env.render()
        action = np.argmax(Q[current_state, :] + noise)
        next_state, reward, done, info = env.step(action)
        Qtarget = reward + discount_factor * np.max(Q[next_state, :])
        Q[current_state, action] = (1-learning_rate) * Q[current_state, action] + learning_rate * Qtarget
        episode_reward += reward
        current_state = next_state
        if done:
          # print('Reward: %s' % episode_reward)
          rewards.append(episode_reward)
          if episode % report_interval == 0:
            print_report(rewards, episode)
          break
        pass
      pass
    env.close()
    print_report(rewards, -1)
    pass
  elif x==4:
    # print('Hopper-v0')
    # print('Least Squares Frozen Lake')
    rewards = []
    n_obs, n_actions = env.observation_space.n, env.action_space.n
    W, Q = initialize((n_obs, n_actions))
    states, labels = [], []
    for episode in range(1, num_episodes+1):
      if len(states) >= 10000:
        states, labels = [], []
        pass
      state = one_hot(env.reset(), n_obs)
      episode_reward = 0
      while True:
        states.append(state)
        noise = np.random.random((1, n_actions)) / episode
        #env.render()
        action = np.argmax(Q(state) + noise)
        state2, reward, done, info = env.step(action)
        state2 = one_hot(state2, n_obs)
        Qtarget = reward + discount_factor * np.max(Q(state2))
        label = Q(state)
        label[action] = (1-learning_rate) * label[action] + learning_rate * Qtarget
        labels.append(label)
        episode_reward += reward
        state = state2
        if len(states) % 10==0:
          W, Q = train(np.array(states), np.array(labels), W)
          pass
        if done:
          rewards.append(episode_reward)
          if episode % report_interval == 0:
            print_report(rewards, episode)
          break
        pass
      pass
    env.close()
    print_report(rewards, -1)
    pass

  elif x==5:
    # print('Space Invaders')
    rewards = []
    for i_episode in range(num_episodes):
      observation = env.reset()
      episode_reward = 0
      while True:
        env.render()
        action = env.action_space.sample()
        observation, reward, done, info = env.step(action)
        episode_reward += reward
        if done:
          print('Reward: %s' % episode_reward)
          rewards.append(episode_reward)
          break 
        pass      
      pass
    env.close()
    print('Average reward: %.2f' % (sum(rewards) / len(rewards)))
    pass
  else:
    # 6
    pass

  return None

def makeQ(model: np.array) -> Callable[[np.array], np.array]:
  """
  Returns a Q-function, which takes state -> distribution over actions.
  """
  return lambda X: X.dot(model)

def one_hot(i: int, n: int) -> np.array:
  """ 
  Implements one-hot encoding by selecting the i-th standard basis vector. 
  """
  return np.identity(n)[i]

def train(X: np.array, y: np.array, W: np.array) -> Tuple[np.array, Callable]:
  """
  Train the model, using the solution to ridge regression.
  """
  I = np.eye(X.shape[1])
  newW = np.linalg.inv(X.T.dot(X) + 10e-4 * I).dot(X.T.dot(y))
  W = w_lr * newW + (1 - w_lr) * W
  Q = makeQ(W)
  return W, Q

def print_report(rewards: List, episode: int):
  """Print rewards report for current episode
  - Average for last 100 episodes
  - Best 100-episode average across all time
  - Average for all episodes across time
  """
  print(report % (np.mean(rewards[-100:]), max([np.mean(rewards[i:i+100]) for i in range(len(rewards) - 100)]), np.mean(rewards), episode))
  pass

def main(x):
  env = game(x)
  loop(x, env)
  return None

if __name__=='__main__':
  g_num=3
  seed = True
  main(g_num)
  pass

#! /usr/bin/env python3.4
from sklearn.datasets import *
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score

# Load Data
data = load_breast_cancer()
label_names = data['target_names']
labels = data['target']
feature_names = data['feature_names']
features = data['data']

# Inspect Data
print(label_names)
print(labels[0])
print(feature_names[0])
print(features[0])

# Splot Data
train, test, train_labels, test_labels = train_test_split(features, labels, test_size=0.33, random_state=42)

# Wori=king with ML Models
gnb = GaussianNB()
model = gnb.fit(train, train_labels)
predicted = gnb.predict(test)
print(predicted)
print(accuracy_score(test_labels, predicted))

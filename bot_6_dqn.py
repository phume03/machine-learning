#! /usr/lib/env python3.5
"""
  Bot 6: Fully featured deep, q-learning network
"""
import cv2 as cv
import gym as game
import numpy as number
import random
import tensorflow as tensor
from bot_6_a3c import a3c_model

random.seed(0)
tensor.set_random_seed(0)
e_num = 10


def downsample(state):
  return cv.resize(state, (84, 84), interpolation=cv.INTER_LINEAR)[None]

def main():
  env = game.make('SpaceInvaders-v0')
  env.seed(0)
  rewards = []
  model = a3c_model(load='models/SpaceInvaders-v0.tfmodel')
  for episode in range(e_num):
    e_reward = 0
    states = [downsample(env.reset())]
    while True:
      if (len(states)<4):
        action = env.action_space.sample()
      else:
        frames = number.concatenate(states[-4:], axis=3)
        action = number.argmax(model([frames]))
      state, reward, done, info = env.step(action)
      states.append(downsample(state))
      e_reward += reward
      if done:
        print('Reward: %d' % e_reward)
        rewards.append(e_reward)
        break
      pass
    pass
  print('Average reward: %.2f' % (sum(rewards)/len(rewards)))
  pass

if __name__=='__main__':
  main()
  pass


#! /usr/lib/env python3.5

""" 
  Tensorflow Indepth: tensorflow by design is a graphing solution. By definition, this means that 'every operation, or computation resides on a graph' (you may not see it right away).
"""

import tensorflow as tensor
import numpy as number

greet1 = tensor.constant("Hello") # Tensorflow constant
greet2 = tensor.constant("World") # Tensorflow constant
greeting = greet1 + " " + greet2 + "!"  # New data structure

# The steps above effectively build a graph - by definition
session = tensor.Session() # Sessions are said to own resources, stored in memory. Thus when we are done with a session, we have to close it!
print(session.run(greeting))
session.close()

x = tensor.Variable([1.,2.,3.,4.,5.,6.], name='y')
with tensor.Session() as session:
  session.run(tensor.global_variables_initializer())
  print(session.run(tensor.reduce_mean(x)))
  pass

session.close()

# ===== GRAPHS ======
trainA = number.linspace(-1,1,101) # line or linear space (c,e,s), produce s elements in the space [c,e].
trainB = 3 * trainA + number.random.randn(*trainA.shape) * 0.33 # produce an array of size *trainA.shape from the random normal distribution. 
# possible subsitute is tf.Variable(tf.random_normal([720,20],stddev=0.09))

A = tensor.placeholder("float")
B = tensor.placeholder("float")
m = tensor.Variable(0.0, name="load")
b_model = tensor.multiply(A,m)
c = (tensor.pow(B-b_model, 2))
t_oper = tensor.train.GradientDescentOptimizer(0.09).minimize(c)

# ==== SESSIONS ====
with tensor.Session() as session:
  session.run(tensor.global_variables_initializer())
  for j in range(100): # 100-iteratoions
    for (a,b) in zip(trainA, trainB): # iterate through A and B graphs
      session.run(t_oper, feed_dict={A: a, B: b})
      print(session.run(m))
      pass
    pass
  pass
session.close()
